/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.storeproject.poc;

import model.Customer;
import model.Product;
import model.Receipt;
import model.User;

/**
 *
 * @author HP
 */
public class TestReceipt {
    public static void main(String[] args){
        Product p1 = new Product(1,"Chayen",30);
        Product p2 = new Product(1,"Americano",40);
        User seller = new User("Piyawan","12345678","password");
        Customer customer = new Customer("Tang","99999999");
        Receipt receipt = new Receipt(seller, customer);
        receipt.addReceiptDetail(p1,1,p1.getPrice());
        receipt.addReceiptDetail(p2,3,p2.getPrice());
        System.out.println(receipt);
        receipt.deleteReceiptDetail(0);
        System.out.println(receipt);
        receipt.addReceiptDetail(p1,2,p1.getPrice());
        System.out.println(receipt);
        
    }
}
